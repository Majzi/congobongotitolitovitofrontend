export const environment = {
  production: true
};

export const backendUrls = {
  LOCAL_HOST: '',
  BASE_END_POINT: '/api',
};
