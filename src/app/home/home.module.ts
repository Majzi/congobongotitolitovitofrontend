import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';


import {FormsModule} from '@angular/forms';
import {HttpInterceptorBasicAuthService} from '../services/http-interceptor-basic-auth.service';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTreeModule} from '@angular/material/tree';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {MatMenuModule} from '@angular/material/menu';
import {MatListModule} from '@angular/material/list';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

@NgModule({
  declarations: [
    HomeComponent,
  ],
  entryComponents: [],
  imports: [
    CommonModule,
    MatDialogModule,
    HttpClientModule,
    HomeRoutingModule,
    FormsModule,
    MatTreeModule,
    MatIconModule,
    MatTableModule,
    MatMenuModule,
    MatListModule,
    MatProgressSpinnerModule,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorBasicAuthService, multi: true}
  ],
})
export class HomeModule { }
