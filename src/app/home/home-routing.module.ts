import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import {RouteGuardService} from '../services/route-guard.service';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent, canActivate:[RouteGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
