import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BasicAuthenticationService} from '../services/basic-authentication.service';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

  constructor(private _http: HttpClient,
              private dialog: MatDialog,
              private basicAuthenticationService: BasicAuthenticationService,
              private router: Router) {
  }

  ngOnInit() {

      // this._http.get(backendUrls.BASE_END_POINT + '/configuration/stockConverter/').subscribe(
      //   data => {
      //
      //   }
      // );
  }
}
