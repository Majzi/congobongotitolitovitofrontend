import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {BasicAuthenticationService} from './basic-authentication.service';
import {catchError, tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {backendUrls} from '../../environments/environment';
// @ts-ignore

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorBasicAuthService implements HttpInterceptor {

  constructor(private basicAuthenticationService: BasicAuthenticationService, private router: Router, private http: HttpClient) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // let basicAuthHEaderString = 'Basic ' + window.btoa(username + ':' + password);
    const basicAuthHeaderString = this.basicAuthenticationService.getAuthenticatedToken();
    const username = this.basicAuthenticationService.getAuthenticatedUser();

    if (basicAuthHeaderString && username) {
      request = request.clone({
        setHeaders: {
          Authorization: basicAuthHeaderString
        }
      });
    }

    return next.handle(request).pipe(catchError(error => {
      // console.log('error');
      if (error instanceof HttpErrorResponse && error.status === 401) {

      } else {
        this.basicAuthenticationService.logout();
        this.router.navigate(['login']);
        return throwError(error);
      }
    }));
  }
}
