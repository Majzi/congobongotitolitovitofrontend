import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {backendUrls} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BasicAuthenticationService {

  constructor(private http: HttpClient) { }

  isUserLoggedIn() {
    const user = sessionStorage.getItem('authenticaterUser')
    return !(user === null);
  }

  getAuthenticatedUser() {
    return  sessionStorage.getItem('authenticaterUser');
  }

  getAuthenticatedToken() {
    if (this.getAuthenticatedUser()) {
      return sessionStorage.getItem('token');
    }
  }

  logout() {
    sessionStorage.removeItem('authenticaterUser');
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('isSestava');
    sessionStorage.removeItem('stores');
  }

  executeJWTAuthenticationService(username, password) {
    return this.http.post<any>(backendUrls.LOCAL_HOST + `/authenticate`, {
      // return this.http.post<any>(`/authenticate`, {
      username,
      password
    }).pipe(
      map(
        data => {
          // console.log(data);
          sessionStorage.setItem('authenticaterUser', username);
          sessionStorage.setItem('token', `Bearer ${data.token}`);

          // this.http.get(backendUrls.BASE_END_POINT + '/configuration/userConfiguration/' + username).subscribe(
          //   dataUser => {
          //     console.log(dataUser);
          //   }
          // );
          // spravil by som to tak ze ak sa user prihlasi tak sa v xml vyhlada user a vyberie sa udaj o informaciach o sestavach a sklade
          return data;
        }
      )
    );
  }
}
